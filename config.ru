require 'rubygems'
require 'bundler'

Bundler.require

require 'sprockets'
map '/assets' do
  environment = Sprockets::Environment.new
  environment.append_path 'assets/stylesheets'
  environment.append_path 'assets/webfonts'
  environment.append_path 'assets/images'
  run environment
end

require './rob-nats-site'
run RobNatsSite